require 'byebug'

# EASY

# Define a method that returns the sum of all the elements in its argument (an
# array of numbers).
def array_sum(arr)
  arr.reduce(0) { |acc, el| acc + el }
end

# Define a method that returns a boolean indicating whether substring is a
# substring of each string in the long_strings array.
# Hint: you may want a sub_tring? helper method
def in_all_strings?(long_strings, substring)
  long_strings.each do |string|
    if !in_string?(string, substring)
      false
    end
  end
  true
end

def in_string? (string, substring)
  string.include? (substring)
end

# Define a method that accepts a string of lower case words (no punctuation) and
# returns an array of letters that occur more than once, sorted alphabetically.
def non_unique_letters(string)
  string_array = string.chars
  y = string_array.select do |el|
    string_array.count(el) > 1
  end
  y.uniq || []
end

# Define a method that returns an array of the longest two words (in order) in
# the method's argument. Ignore punctuation!
def longest_two_words(string)
  string.delete! (".?!,:;")
  words = string.split
  ord = words.sort_by{|word|
    word.length}.reverse!
  ord[0..1].reverse!
end

# MEDIUM

# Define a method that takes a string of lower-case letters and returns an array
# of all the letters that do not occur in the method's argument.
def missing_letters(string)
  ("a".."z").to_a.reject do |el|
    string.include? (el)
  end
end

# Define a method that accepts two years and returns an array of the years
# within that range (inclusive) that have no repeated digits. Hint: helper
# method?
def no_repeat_years(first_yr, last_yr)
  (first_yr .. last_yr).select {|yr|
    not_repeat_year?(yr)}
end

def not_repeat_year?(year)
  y = year.to_s.chars
  y == y.uniq
end

# HARD

# Define a method that, given an array of songs at the top of the charts,
# returns the songs that only stayed on the chart for a week at a time. Each
# element corresponds to a song at the top of the charts for one particular
# week. Songs CAN reappear on the chart, but if they don't appear in consecutive
# weeks, they're "one-week wonders." Suggested strategy: find the songs that
# appear multiple times in a row and remove them. You may wish to write a helper
# method no_repeats?
def one_week_wonders(songs)
  songs.select do |song|
    no_repeats? (song, songs)
  end
end

def no_repeats?(song_name, songs)
  songs.each_with_index do |el, i|
    if el == songs[i+1]
      false
    end
  end
  true
end

# Define a method that, given a string of words, returns the word that has the
# letter "c" closest to the end of it. If there's a tie, return the earlier
# word. Ignore punctuation. If there's no "c", return an empty string. You may
# wish to write the helper methods c_distance and remove_punctuation.

def for_cs_sake(string)
  words = string.delete(".?!,:;").split
  cword = ''
  words.each { |word|
    if (c_distance(word) != nil)
      if cword == '' || (c_distance(word) < c_distance(cword))
        cword = word
      end
    end
  }
  cword
end

def c_distance(word)
  word.downcase.reverse.index('c')
end

# Define a method that, given an array of numbers, returns a nested array of
# two-element arrays that each contain the start and end indices of whenever a
# number appears multiple times in a row. repeated_number_ranges([1, 1, 2]) =>
# [[0, 1]] repeated_number_ranges([1, 2, 3, 3, 4, 4, 4]) => [[2, 3], [4, 6]]

def repeated_number_ranges(arr)
  result = []
  start = nil
  en = nil
  num = nil
  arr.each_with_index { |n, i|
    if num == nil || num != n
      if en != nil
        result.push([start,en])
        en = nil
      end
      num = n
      start = i
    elsif num == n
      en = i
    end
  }
  if en != nil
    result.push([start,en])
  end
  result
end
